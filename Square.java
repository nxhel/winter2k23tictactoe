public enum Square{
  X,O,BLANK;
  
  public String modifyValue()
  {
    String value=" ";
    if(this == Square.X)
    {
      value="X";
    }
    if(this == Square.O)
    {
      value="O";
    }
    if(this == Square.BLANK)
    {
      value="_";
    }
    return value;
  }
  
   public String toString()
   {
     return modifyValue();
   }
}
