import java.util.Scanner;

public class TicTacToeGame {
  
  public static void main(String [] args){
    
    
    //Welcome Messages
    System.out.println("WELCOME TO NIHEL'S EPIC TIC TAC TOE GAME!!!");
     System.out.println(" ");
    System.out.println("Player 1 you will have the F A T A L  TOKEN X");
    System.out.println("Player 2 you will have the U N B R E A K A B L E  TOKEN O");
    
    Board theBoard = new Board();
    
    boolean gameOver=false;
    int player=1;
    Square playerToken=Square.X;
    
    Scanner scan= new Scanner(System.in);
    
    while(!gameOver)
    {
      System.out.println(theBoard);
      
      if(player==1)
      {
        System.out.println("Player 1: Time to attack");
        playerToken=Square.X;
      }
      else
      {
        System.out.println("Player 2: Time to counter-attack");
        playerToken=Square.O;
      }
      
    
      
        System.out.println("PLAYER " + player + ": enter your ATTACK move starting by the row than column ");
          int row= scan.nextInt()-1;
          int col= scan.nextInt()-1;
      
      while(!theBoard.placeToken(row, col, playerToken))
      {
        System.out.println("The position you entered is invalid... Re-enter another position");
        row=scan.nextInt()-1;
        col= scan.nextInt()-1;
      }

 
       if(theBoard.checkIfFull())
       {
         System.out.println(theBoard); 
         System.out.println("It's a tie!");
         gameOver=true;
       }
      else if(theBoard.checkIfWinning(playerToken))
      {
       System.out.println(theBoard); 
       System.out.println("Player "+ player+" YOU ARE THE LUCKY WINNER ;))!");
       gameOver=true;
      }
     else 
     { 
      player++;
      if(player>2){
        player=1;
      }  
    }
    
   }
      scan.close();
  }

}

      
        

    
