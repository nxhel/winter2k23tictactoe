import java.util.Scanner;
public class Board
{
  private Square[][] tictactoeBoard;
  
  public Board()
  {
    Scanner scan= new Scanner(System.in);
    //Input From User To Customize the Board
    System.out.println("Pleaser enter the number of rows and colums of THE BOARD ");
    final int NUM_ROWS=scan.nextInt();
    final int NUM_COLUMNS=scan.nextInt();
    
    //initializing and declaring what inside the 2D ARRAY
    this.tictactoeBoard= new Square[NUM_ROWS][NUM_COLUMNS];
    
    for(int i = 0; i<this.tictactoeBoard.length; i++)
    {
      for(int j = 0; j<this.tictactoeBoard[i].length; j++)
      {
        this.tictactoeBoard[i][j] = Square.BLANK;
      }
    }
      scan.close();
    }
 public String PrintBoard()
 {

   String boardPrinting=" ";
    for(int i=0; i<this.tictactoeBoard.length;i++)
    {
       for (int j=0;j<this.tictactoeBoard[i].length;j++)
       {
         boardPrinting+=this.tictactoeBoard[i][j]+ " ";
       }
       boardPrinting+= " \n ";
    }
    return boardPrinting;
  }

  public String toString()
  {
    return PrintBoard();
  }

public Boolean placeToken(int row, int col, Square playerToken)
{
 
  if (row<0 && row>2 && col<0 && col>2)
  {
    return false;
  }
    if(this.tictactoeBoard[row][col]==Square.BLANK)
    {
      this.tictactoeBoard[row][col]=playerToken;
      return true;
    }
   
    return false;
}

public Boolean checkIfFull()
{
 for(int i=0; i<this.tictactoeBoard.length;i++)
    {
       for (int j=0;j<tictactoeBoard[i].length;j++)
       {
         if(this.tictactoeBoard[i][j]==Square.BLANK)
         {
           return false;
         }
       }
 }
 return true;
}


private boolean checkIfWinningHorizontal(Square playerToken)
{
    for(int i=0; i<this.tictactoeBoard.length;i++)
    {
      if(tictactoeBoard[i][0]==playerToken)
      {
        if (tictactoeBoard[i][1]==playerToken)
        {
          if(tictactoeBoard[i][2]==playerToken) 
          {
            return true;
          }
        }
      }
    }
    
    return false;
  }

private boolean checkIfWinningVetical(Square playerToken)
{
    
    for(int j=0; j<this.tictactoeBoard.length;j++)
    {
      if(tictactoeBoard[0][j]==playerToken)
      {
        if( tictactoeBoard[1][j]==playerToken)
        {
          if(tictactoeBoard[2][j]==playerToken)
          {   
            return true;
          }
        }
      }
    }
    return false;
  }


  private boolean checkIfWinningDiagonal( Square playerToken){
    
    if (tictactoeBoard[0][0] == playerToken)
    {
       if( tictactoeBoard[1][1]== playerToken)
       {
           if(tictactoeBoard[2][2]== playerToken)
           {
             return true;
           }
       }
    }
     
      if (tictactoeBoard[0][2] == playerToken)
      {
        if(tictactoeBoard[1][1]== playerToken)
        {
           if(tictactoeBoard[2][0]== playerToken)
           {
             return true;
           }
        }
      }
      return false;
   }

public boolean checkIfWinning (Square playerToken)
   {
     if (checkIfWinningHorizontal(playerToken) ||
         checkIfWinningVetical(playerToken)||
         checkIfWinningDiagonal(playerToken)){
       return true;
     }
     return false;
   }
}






  